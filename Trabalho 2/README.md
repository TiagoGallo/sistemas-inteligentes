# Trabalho 2

Treinamento e teste de uma rede perceptron multi camadas na base de dados MNIST

## Dependências

Para rodar esses programas, precisa ter instalado

* python >= 3.5.2
* Numpy
* Matplotlib (se voce desejar gerar as imagens de erro e acurácia)
* torch
* torchvision

Instalação do pytorch: [link](https://pytorch.org/get-started/locally/)

### Uso

```bash

python treinamento.py [-h] [--lr LR] [--epochs EPOCHS] [--verbose VERBOSE]
                      [--show-fig] [--validation-split VALIDATION_SPLIT]
                      [--batch BATCH] [--l1] [--l2]
                      [--norm-weight NORM_WEIGHT] [--estrutura {1,2}]
                      [--momentum MOMENTUM] [--dropout]
                      [--dropout_p DROPOUT_P] [--data_augmentation]

```

A opção -h ou --help mostra todos os parâmetros possíveis para rodar esse programa e uma ajuda de pra que cada um serve

**Parâmetros**

* **--epochs** -> Determina quantas épocas serão utilizadas para fazer o treinamento. (Valor padrão = 20).
* **--lr** -> Determina o valor da taxa de aprendizado para o treinamento. (Valor padrão = 0.01).
* **--show-fig** -> Se esse parâmetro for passado, o programa vai gerar as imagens de erro e acurácia do treinamento. (Você precisa do pacote matplotlib para usar esse parâmetro).
* **--validation-split** -> Determina qual percentual das imagens de treino que serão utilizadas para a validação. (Valor padrão = 20).
* **--verbose** -> Determina de quantas em quantas épocas os resultados intermediários serão impressos no terminal. (Valor padrão = 12000).
* **--batch** -> Tamanho do batch de imagens para o treinamento.
* **--l1** -> Se esse parâmetro for acionado, será usada a regularização L1 (apenas na camada de saída da rede) durante o treinamento.
* **--l2** -> Se esse parâmetro for acionado, será usada a regularização L2 (apenas na camada de saída da rede) durante o treinamento.
* **--norm-weight** -> Peso associado a normalizacao L1 ou L2. (Valor padrão = 0.2)
* **--estrutura** -> Algumas estruturas de rede foram pré-definidas no arquivo NeuralNetwork.py, esse parâmetro indica qual o número da estrutura que será utilizada. 
* **--momentum** -> Valor do momentum a ser adicionado no otmizador por descida de gradiente. (Valor padrão = 0).
* **--dropout** -> Se ativado vai adicionar uma 'camada' de dropout antes da camada de saida.
* **--dropout_p** -> Probabilidade do peso ser zerado pela camada de dropout. (valor padrão = 0.5)
* **--data-augmentation** -> Se ativado vai fazer algumas transformacoes nas imagens do dataset de treino, para simular que temos mais imagens.

### Dica

Utilize o seguinte comando para alcançar, mais ou menos, 98.5% de acurácia no conjunto de testes:

```bash

python treinamento.py --estrutura 2 --show-fig --epochs 30 --dropout --momentum 0.5 --data_augmentation

```