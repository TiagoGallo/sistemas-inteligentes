import struct
import matplotlib.pyplot as plt
import numpy as np

def read_imagesfile(filename):
    #filename = "./data/train/train-images.idx3-ubyte"

    file = open(filename, 'rb') #abre o arquivo
    file.seek(0)    #seta o ponteiro do arquivo pra posicao inicial

    magic = struct.unpack('>4B',file.read(4)) # > is big-endian, B is unsigned char
    print("magic number: ",magic)

    nImg = struct.unpack('>I',file.read(4))[0] #num of images
    nR = struct.unpack('>I',file.read(4))[0] #num of rows
    nC = struct.unpack('>I',file.read(4))[0] #num of column

    print("numero de imagens: ", nImg)
    print("numero de linhas: ", nR)
    print("numero de colunas: ", nC)

    images_array = np.zeros((nImg, nR, nC))
    nBytesTotal = nImg*nR*nC*1 #since each pixel data is 1 byte
    images_array = 255 - np.asarray(struct.unpack('>'+'B'*nBytesTotal,file.read(nBytesTotal))).reshape((nImg,nR,nC))
    #print("images array: ", images_array)

    file.close()
    return images_array

def read_labelfile(filename):
    #filename = "./data/train/train-labels.idx1-ubyte"

    file = open(filename, 'rb') #abre o arquivo
    file.seek(0)    #seta o ponteiro do arquivo pra posicao inicial

    magic = struct.unpack('>4B',file.read(4)) # > is big-endian, B is unsigned char
    print("magic number: ",magic)

    nLabels = struct.unpack('>I',file.read(4))[0] #num of images
    print("numero de labels: ", nLabels)

    labels = []

    for _ in range(0, nLabels):
        Label = struct.unpack('>B',file.read(1))[0] #num of images
        labels.append(Label)

    return labels

def visualization(type, n_samples = 10):
    '''
    Funcao para visualizar os dados

    type: pode ser treino ou teste
    n_samples: quantas imagens serao visualizadas

    '''
    if type == 'treino':
        images_array = read_imagesfile("./data/train/train-images.idx3-ubyte")
        labels = read_labelfile("./data/train/train-labels.idx1-ubyte")
    elif type == 'teste':
        images_array = read_imagesfile("./data/test/t10k-images.idx3-ubyte")
        labels = read_labelfile("./data/test/t10k-labels.idx1-ubyte")
    else:
        raise TypeError("O tipo passado so pode ser treino ou teste")

    counter = 0
    for img, label in zip(images_array, labels):
        counter += 1

        # Plot
        plt.title('Label is {label}'.format(label=label))
        plt.imshow(img, cmap='gray')
        plt.show()

        if counter == n_samples:
            break

visualization("teste", 10)