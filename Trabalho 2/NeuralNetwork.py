import torch
import torch.nn as nn
import torch.nn.functional as F

class MLP_1(nn.Module):
    '''
    Nossa classe definindo o multi layer perceptron  (estrutura 1)
    '''
    def __init__(self, use_dropout, dropout_p):
        super(MLP_1, self).__init__()
        self.use_dropout = use_dropout

        # 1 input image that is 28x28 pixels
        self.input_layer = nn.Linear(28 * 28, 120)      #camada de entrada
        self.hidden_1 = nn.Linear(120, 84)           #camada escondida com 120 perceptrons
        self.hidden_2 = nn.Linear(84, 48)            #camada escondida com 84 perceptrons
        
        if self.use_dropout:
            self.dropout_layer = nn.Dropout(p = dropout_p)
        
        self.final_layer = nn.Linear(48, 10)

    def forward(self, x):
        x = x.view(-1, 28 * 28)
        x = F.relu(self.input_layer(x))
        x = F.relu(self.hidden_1(x))
        x = F.relu(self.hidden_2(x))

        if self.use_dropout:
            x = self.dropout_layer(x)

        x = F.log_softmax(self.final_layer(x), dim=1)
        return x

class MLP_2(nn.Module):
    '''
    Nossa classe definindo o multi layer perceptron (estrutura 2)
    Inspirada no artigo Deep Big Simple Neural Nets Excel on Handwritten Digit Recognition
    '''
    def __init__(self, use_dropout, dropout_p):
        super(MLP_2, self).__init__()
        self.use_dropout = use_dropout
        
        # 1 input image that is 28x28 pixels
        self.input_layer = nn.Linear(28 * 28, 1000)      #camada de entrada
        self.hidden_1 = nn.Linear(1000, 1000)            #camada escondida com 1000 perceptrons
        self.hidden_2 = nn.Linear(1000, 1000)            #camada escondida com 1000 perceptrons
        self.hidden_3 = nn.Linear(1000, 1000)            #camada escondida com 1000 perceptrons
        self.hidden_4 = nn.Linear(1000, 1000)            #camada escondida com 1000 perceptrons

        if self.use_dropout:
            self.dropout_layer = nn.Dropout(p = dropout_p)

        self.final_layer = nn.Linear(1000, 10)

    def forward(self, x):
        x = x.view(-1, 28 * 28)
        x = F.relu(self.input_layer(x))
        x = F.relu(self.hidden_1(x))
        x = F.relu(self.hidden_2(x))
        x = F.relu(self.hidden_3(x))
        x = F.relu(self.hidden_4(x))
        
        if self.use_dropout:
            x = self.dropout_layer(x)
        
        x = F.log_softmax(self.final_layer(x), dim=1)
        return x

class MLP_3(nn.Module):
    '''
    Nossa classe definindo o multi layer perceptron (estrutura 3)
    Inspirada no artigo Deep Big Simple Neural Nets Excel on Handwritten Digit Recognition
    '''
    def __init__(self, use_dropout, dropout_p):
        super(MLP_3, self).__init__()
        self.use_dropout = use_dropout
        
        # 1 input image that is 28x28 pixels
        self.input_layer = nn.Linear(28 * 28, 2500)      #camada de entrada
        self.hidden_1 = nn.Linear(2500, 2000)            #camada escondida com 1000 perceptrons
        self.hidden_2 = nn.Linear(2000, 1500)            #camada escondida com 1000 perceptrons
        self.hidden_3 = nn.Linear(1500, 1000)            #camada escondida com 1000 perceptrons
        self.hidden_4 = nn.Linear(1000, 500)             #camada escondida com 1000 perceptrons

        if self.use_dropout:
            self.dropout_layer = nn.Dropout(p = dropout_p)

        self.final_layer = nn.Linear(500, 10)

    def forward(self, x):
        x = x.view(-1, 28 * 28)
        x = F.relu(self.input_layer(x))
        x = F.relu(self.hidden_1(x))
        x = F.relu(self.hidden_2(x))
        x = F.relu(self.hidden_3(x))
        x = F.relu(self.hidden_4(x))
        
        if self.use_dropout:
            x = self.dropout_layer(x)
        
        x = F.log_softmax(self.final_layer(x), dim=1)
        return x

class MLP_4(nn.Module):
    '''
    Nossa classe definindo o multi layer perceptron (estrutura 4)
    Teste sem camada escondida
    '''
    def __init__(self, use_dropout, dropout_p):
        super(MLP_4, self).__init__()
        self.use_dropout = use_dropout
        
        # 1 input image that is 28x28 pixels
        self.final_layer = nn.Linear(28 * 28, 10)      #camada de entrada

        if self.use_dropout:
            self.dropout_layer = nn.Dropout(p = dropout_p)

    def forward(self, x):
        x = x.view(-1, 28 * 28)
        
        if self.use_dropout:
            x = self.dropout_layer(x)
        
        x = F.log_softmax(self.final_layer(x), dim=1)
        return x