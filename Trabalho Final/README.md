# Trabalho Final

Treinamento de uma cycleGAN para transformar imagens de rostos em retratos falados e vice-versa.

Forked from [here](https://github.com/eriklindernoren/PyTorch-GAN/tree/master/implementations/cyclegan)

Dataset: CUHK Face Sketch (student dataset) -> [Download](http://mmlab.ie.cuhk.edu.hk/archive/facesketch.html)

Codigo e ideia retirada deste [paper](http://openaccess.thecvf.com/content_ICCV_2017/papers/Zhu_Unpaired_Image-To-Image_Translation_ICCV_2017_paper.pdf)

Redes pré-treinadas -> [Download](https://drive.google.com/drive/folders/1ISbWbN6K2RQpIvkGa7BtIGuyPl2SH41a?usp=sharing)

## Dependências

Para rodar esses programas, precisa ter instalado

* python >= 3.5.2
* Numpy
* Matplotlib (se voce desejar gerar as imagens de erro e acurácia)
* torch
* torchvision

Instalação do pytorch: [link](https://pytorch.org/get-started/locally/)

### Uso - arquivo de treinamento

```bash

cyclegan.py [-h] [--epoch EPOCH] [--n_epochs N_EPOCHS]
                   [--dataset_name DATASET_NAME] [--batch_size BATCH_SIZE]
                   [--lr LR] [--b1 B1] [--b2 B2] [--decay_epoch DECAY_EPOCH]
                   [--n_cpu N_CPU] [--img_height IMG_HEIGHT]
                   [--img_width IMG_WIDTH] [--channels CHANNELS]
                   [--sample_interval SAMPLE_INTERVAL]
                   [--checkpoint_interval CHECKPOINT_INTERVAL]
                   [--n_residual_blocks N_RESIDUAL_BLOCKS]
                   [--lambda_cyc LAMBDA_CYC] [--lambda_id LAMBDA_ID]
                   [--resume RESUME]

```

A opção -h ou --help mostra todos os parâmetros possíveis para rodar esse programa e uma ajuda de pra que cada um serve


### Uso - arquivo de aplicação da rede para visualização das imagens

```bash

apply_ciclegan.py [-h] [--resume RESUME] [--mode {0,1}]
                         [--img_path IMG_PATH]

```

A opção -h ou --help mostra todos os parâmetros possíveis para rodar esse programa e uma ajuda de pra que cada um serve

* --resume RESUME      batch_epoch to resume from
* --mode {0,1}         Mode 0 do sketch to face; Mode 1 do face to sketch.
* --img_path IMG_PATH  path to the image to be transformed