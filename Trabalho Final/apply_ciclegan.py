import argparse
import torch
import torchvision.transforms as transforms
from PIL import Image
from torchvision.utils import save_image
from torch.autograd import Variable
from models import *

parser = argparse.ArgumentParser()
parser.add_argument("--resume", type = str, default = "151_13360", help = "batch_epoch to resume from")
parser.add_argument("--mode", type = int, default = 0, choices = [0, 1], help = "Mode 0 do sketch to face; \tMode 1 do face to sketch.")
parser.add_argument("--img_path", type = str, help = "path to the image to be transformed")
opt = parser.parse_args()

cuda = torch.cuda.is_available()

Tensor = torch.cuda.FloatTensor if cuda else torch.Tensor

class CycleGAN(object):
    def __init__(self, opt, cuda):
        input_shape = (3, 252, 250)

        # Initialize generator
        self.G = GeneratorResNet(input_shape, 9)

        if cuda:
            self.G = self.G.cuda()

        if opt.mode == 0:
            #load sketch to face model
            self.G.load_state_dict(torch.load("saved_models/sketch2face/G_AB_%s.pth" % (opt.resume)))
        elif opt.mode == 1:
            #load sketch to face model
            self.G.load_state_dict(torch.load("saved_models/sketch2face/G_BA_%s.pth" % (opt.resume)))

    def __call__(self, img):
        #evaluation mode
        self.G.eval()

        #expand img dims
        img = img[None,:,:,:]

        #Put image in a Variable 
        real_img = Variable(img.type(Tensor))

        #generate the output
        return self.G(real_img) 

    def __repr__(self):
        return "CycleGAN"

transforms_ = transforms.Compose([
    transforms.Resize((252, 250)),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
    CycleGAN(opt, cuda)
])

#load the original image
real_img = Image.open(opt.img_path).convert("RGB")

#save the original img
real_img.save("./figs/original.png")

#Apply the GAN
transformed_img = transforms_(real_img)

#Save the GAN output
save_image(transformed_img, "./figs/transformed.png", normalize=True)