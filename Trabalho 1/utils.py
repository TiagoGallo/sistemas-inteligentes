import csv

def read_csv_file(filetype):
    '''
    Funcao para ler os dados do arquivo csv

    Vamos definir nessa funcao que quando a saida for mina (M) temos o valor 0.0
e quando a saida for rocha (R) temos o valor 1.0

    Entradas:
        filetype - String que determina se eh para ler os dados
do arquivo de teste ou de treinamento. Valores aceitos: "teste", "treino"

    Saidas:
        data - Lista contendo os dados lidos do arquivo CSV
    '''
    if filetype == "teste":
        filename = 'sonar.test-data'
    elif filetype == "treino":
        filename = 'sonar.train-data'
    else:
        raise ValueError ("Essa funcao so aceita como entrada as strings "
            "'treino' ou 'teste'. O valor {} e invalido.".format(filetype))
    
    #Abre o arquivo e faz a leitura dele 
    csv_file = open(filename)
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0

    #Prepara a lista de retorno
    data = []

    #Itera pelas linhas do arquivo, convertendo os dados e preenchendo a lista de retorno
    for row in csv_reader:
        line_count += 1

    #Converte os valores de string para float    
        for i in range(0, len(row) - 1):
            row[i] = float(row[i])
        if row[-1] == 'M':
            row[-1] = 0.0
        elif row[-1] == "R":
            row[-1] = 1.0
        else:
            raise ValueError("Existe um erro no ultimo elemento da linha {} "
                "do arquivo {}. O elemento no seu arquivo e {} e so poderia "
                "ser M ou R".format(line_count, filename, row[-1]))

        #Insere a linha de dados na lista de retorno
        data.append(row)

    #Fecha o arquivo e retorna a lista com os valores 
    csv_file.close()
    return data
