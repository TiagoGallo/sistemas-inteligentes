from utils import read_csv_file
from perceptron import Perceptron
import numpy as np
import argparse

def main(args):
    '''
    Metodo principal do programa
    '''
    #Le o arquivo de entrada para pegar os dados de treinamento
    dados_de_treinamento = read_csv_file("treino")

    #Cria um perceptron com 60 entradas para o problema
    percep = Perceptron(60)

    #Roda o treinamento no perceptron criado com os argumentos da entrada do usuario e os dados de treinamento
    resultado_treinamento = treinamento(args, percep, dados_de_treinamento)

    #Le o arquivo de teste e calcula a acuracia do teste
    dados_de_teste = read_csv_file("teste")

    #Roda o teste com os melhores pesos encontrados na validacao do treinamento
    resultados_teste = teste(percep, dados_de_teste, resultado_treinamento)

    if args.show_fig:
        plot_figs(resultado_treinamento)

def treinamento(args, percep, dados_de_treinamento):
    '''
    Metodo que realiza o treinamento do modelo a partir dos dados de treinamento
    '''
    #carrega os parametros de treinamento passados pelo usuario
    learning_rate = args.lr
    epochs = args.epochs
    verbose = args.verbose
    percentual_validation = args.validation_split

    #Da linha de treinamento 1 ate esse indice os dados vao para treinamento, o restantes das linhas vao para validacao
    indice_max_treinamento = int(((1 - (percentual_validation / 100))* len(dados_de_treinamento)))

    #cria um dicionario para armazenar os resultados do treinamento
    resultado_treinamento = {
        'best_weights': [],
        'min_error_valid': 1000000000,
        'min_error_train': 1000000000,
        'best_epoch': 0,
        'epochs_list': [],
        'acc_list_train': [],
        'acc_list_valid': [],
        'error_list_train': [],
        'error_list_valid': [],
        'best_acc_valid': 0,
        'linhas_train': 0,
        'linhas_valid': 0
    }
    
    #Itera pelos dados de treinamento no numero de epocas desejadas
    for i in range(epochs):
        #zera os parametros que ajudam a calcular os valores de erro e acuracia em cada epoca de treinamento
        total_linhas_train = 0
        total_acertos_train = 0
        erro_quadratico_acumulado_train = 0

        #zera os parametros que ajudam a calcular os valores de erro e acuracia em cada epoca de validacao
        total_linhas_valid = 0
        total_acertos_valid = 0
        erro_quadratico_acumulado_valid = 0

        #itera os dados de treinamento para a epoca atual
        for num_linha, linha in enumerate(dados_de_treinamento):
            #Verifica se esse dado deve ser usado para treinamento ou validacao
            if (num_linha + 1) <= indice_max_treinamento:
                total_linhas_train += 1
                erro_pontual = percep.update(linha, learning_rate)
                
                #analisa se o perceptron errou ou acertou a predicao. Caso tenha acertado, adiciona +1 no acumulador de 
                #acertos, caso tenha errado adiciona o valor do erro ao quadrado no erro quadratico acumalado
                if erro_pontual == 0.0:
                    total_acertos_train += 1
                else:
                    erro_quadratico_acumulado_train += np.power(erro_pontual, 2)

            else:
                total_linhas_valid += 1

                #Prepara o dado pra validacao
                dados = linha[0:-1]
                dados.append(1.0)
                
                #Faz a predicao do resultado
                erro_pontual = percep.predict(dados) - linha[-1] 

                if erro_pontual == 0.0:
                    total_acertos_valid += 1
                else:
                    erro_quadratico_acumulado_valid += np.power(erro_pontual, 2)


        #Calcula o erro quadratico da epoca e adiciona ao vetor de erros
        erro_quadratico_train = erro_quadratico_acumulado_train / 2
        erro_quadratico_valid = erro_quadratico_acumulado_valid / 2

        #Calcula a acuracia da epoca atual
        acc_atual_train = total_acertos_train / total_linhas_train
        acc_atual_valid = total_acertos_valid / total_linhas_valid

        #Se o erro da epoca atual for menor que o menor erro ate entao, atualiza os resultados 
        if ((erro_quadratico_valid) < resultado_treinamento['min_error_valid']) or ((erro_quadratico_valid == resultado_treinamento['min_error_valid']) and erro_quadratico_train < resultado_treinamento['min_error_train']):
            resultado_treinamento['best_epoch'] = i + 1
            resultado_treinamento['best_weights'] = percep.pesos
            resultado_treinamento['best_acc_valid'] = acc_atual_valid
            resultado_treinamento['min_error_valid'] = erro_quadratico_valid

        if erro_quadratico_train < resultado_treinamento['min_error_train']:
            resultado_treinamento['min_error_train'] = erro_quadratico_train

        #Verifica se ha necessidade de printar os resultados da epoca atual
        if (i+1) % verbose == 0 or erro_quadratico_train == 0.0 or i == 0 or erro_quadratico_valid == 0.0 or (i + 1 == epochs):
            print("Fim da epoch {}\nTreino ===> Acc = {:.2f}% "
                "Erro = {}\nValidacao ===> Acc = {:.2f}% "
                "Erro = {}\n".format(i + 1, 
                (acc_atual_train) * 100, erro_quadratico_train,
                (acc_atual_valid * 100), erro_quadratico_valid
            ))

            #Adiciona os resultados dessa epoca nas listas de resultado
            resultado_treinamento['epochs_list'].append(i+1)
            resultado_treinamento['acc_list_train'].append(acc_atual_train)
            resultado_treinamento['acc_list_valid'].append(acc_atual_valid)
            resultado_treinamento['error_list_train'].append(erro_quadratico_train)
            resultado_treinamento['error_list_valid'].append(erro_quadratico_valid)

        #Se o erro de treinamento tiver atingido 0, nao ha mais necessidade de treinamento, entao encerra o loop
        if erro_quadratico_train == 0.0 or erro_quadratico_valid == 0.0:
            print("Acuracia de treinamento esta em 100%, entao nao ha mais pq continuar treinando")
            break

    #Salva quantas linhas foram usadas para treinamento e quantas foram usadas para validacao
    resultado_treinamento['linhas_train'] = total_linhas_train
    resultado_treinamento['linhas_valid'] = total_linhas_valid

    print("A melhor epoch foi a {} com uma acuracia de {:.2f}% na validacao".format(
        resultado_treinamento['best_epoch'], resultado_treinamento['best_acc_valid'] * 100
    ))

    return resultado_treinamento

def teste(percep, dados_de_teste, resultado_treinamento):
    '''
    Metodo que realiza o teste do modelo a partir dos dados de teste e dos melhores pesos encontrados no treinamento
    M = 0.0
    R = 1.0
    '''
    #Cria um dicionario para armazenar os resultados do teste
    resultados_teste = {
        "acc": 0,
        "erro": 0,

        "rochas_certas": 0,
        "rochas_erradas": 0,
        "minas_certas": 0,
        "minas_erradas": 0
    }

    #Parametros para ajudar a calculas os resultados do teste
    total_linhas = 0
    total_acertos = 0
    erro_quad_acumulado = 0

    #atualiza os pesos do perceptron com os pesos referentes a melhor epoca no treinamento
    percep.pesos = resultado_treinamento['best_weights']

    for linha in dados_de_teste:
        total_linhas += 1
        
        #retira as informacoes da linha de teste e adiciona o 1.0 para a entrada do bias
        dados = linha[0:-1]
        dados.append(1.0)

        #Faz a predicao do resultado
        predicao = percep.predict(dados)

        #Calcula o erro
        erro = predicao - linha[-1]

        #analisa o erro e incrementa os acertos para um erro = 0, e incrementa o erro quadratico acumulado para erro != 0
        if erro == 0.0:
            total_acertos += 1

            #Analisa se o acerto foi de uma mina ou rocha
            if linha[-1] == 0.0:
                resultados_teste["minas_certas"] += 1
            else:
                resultados_teste["rochas_certas"] += 1

        else:
            erro_quad_acumulado += np.power(erro, 2)

            #Analisa se o erro foi de uma mina ou rocha
            if linha[-1] == 0.0:
                resultados_teste["minas_erradas"] += 1
            else:
                resultados_teste["rochas_erradas"] += 1

    #Calcula a acc e o erro quadratico no conjunto de testes
    resultados_teste['acc'] = total_acertos / total_linhas
    resultados_teste['erro'] = erro_quad_acumulado / 2

    print("\nO resultado no conjunto de testes:\nAcuracia: {}/{} = {:.2f}%\n"
    "Erro quadratico: {}\n".format(total_acertos, total_linhas, 
        resultados_teste['acc'] * 100, resultados_teste['erro']
    ))

    print("Matriz de confusao:\n\n"
        "\t ||\tPredito como Rocha ||\tPredito como Mina ||\n"
        "Rocha    ||\t\t{:02d}\t   ||\t\t{:02d}\t  ||\n"
        "Mina     ||\t\t{:02d}\t   ||\t\t{:02d}\t  ||\n".format(
            resultados_teste["rochas_certas"], resultados_teste["rochas_erradas"],
            resultados_teste["minas_erradas"], resultados_teste["minas_certas"],
        ))

    return resultados_teste

def plot_figs(resultado_treinamento):
    '''
    Metodo para gerar graficos com os resultados do treinamento
    '''
    #Importa a biblioteca de plots
    import matplotlib.pyplot as plt

    #Gera uma lista com valores para representar uma linha de acuracia 1.0
    linha = [1.0] * len(resultado_treinamento['epochs_list'])

    plt.subplot(2, 1, 1)
    plt.plot(resultado_treinamento['epochs_list'], linha, ls='--', color='black', label='acc = 100%')
    plt.plot(resultado_treinamento['epochs_list'], resultado_treinamento['acc_list_train'], label='Acc Treino', color='blue')
    plt.plot(resultado_treinamento['epochs_list'], resultado_treinamento['acc_list_valid'], label='Acc Validacao', color='red')

    plt.xlabel('epochs')
    plt.ylabel('Accuracy')

    plt.title("Accuracy")
    plt.legend()

    plt.subplot(2, 1, 2)

    #Normaliza os dados dos erros de treino e de validacao para o grafico fazer mais sentido
    resultado_treinamento['error_list_train'] = np.divide(resultado_treinamento['error_list_train'], resultado_treinamento['linhas_train']).tolist()
    resultado_treinamento['error_list_valid'] = np.divide(resultado_treinamento['error_list_valid'], resultado_treinamento['linhas_valid']).tolist()

    plt.plot(resultado_treinamento['epochs_list'], resultado_treinamento['error_list_train'], label='Erro Treino', color='blue')
    plt.plot(resultado_treinamento['epochs_list'], resultado_treinamento['error_list_valid'], label='Erro Validacao', color='red')
    plt.xlabel('epochs')
    plt.ylabel('Erro Quadratico Normalizado')

    plt.title("Erro")
    plt.legend()

    plt.subplots_adjust(hspace = .5)

    plt.show()

if __name__ == "__main__":
    #Create a argument parser and parse the arguments
    parser = argparse.ArgumentParser()

    #Variables for the video source
    parser.add_argument("--lr", type = float, help = "O valor da taxa de aprendizado (learning rate) para realizar o treinamento",
                        default = 0.001)
    parser.add_argument("--epochs", type = int, help = "O numero maximo de epocas que o treinamento pode realizar", 
                        default = 10000)
    parser.add_argument("--verbose", type = int, default = 500,
                        help = "Numero que define de quantas em quantas epocas o usuario quer um print de feedback do programa")
    parser.add_argument("--show-fig", action = 'store_true',
                        help = "Se esse argumento for passado, o programa vai gerar imagens sobre a acuracia e o erro durante o treinamento")
    parser.add_argument("--validation-split", type = int, default = 20,
                        help = "Define qual percentual do conjunto de treinamento deve ser usado para a validacao")

    args = parser.parse_args()

    main(args)