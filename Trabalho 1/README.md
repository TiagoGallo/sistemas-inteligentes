# Trabalho 1

Treinamento e teste de uma rede perceptron na base de dados "Sonar"

## Dependências

Para rodar esses programas, precisa ter instalado

* python >= 3.5.2
* Numpy
* Matplotlib (se voce desejar gerar as imagens de erro e acurácia)

### Uso

```bash

python treinamento.py [-h] [--epochs NUM_EPOCAS] [--lr TAXA_DE_APRENDIZADO]
        [--show-fig] [--validation-split PERCENTUAL_VALIDACAO]
        [--verbose NUM_VERBOSE]

```

A opção -h ou --help mostra todos os parâmetros possíveis para rodar esse programa e uma ajuda de pra que cada um serve

**Parâmetros**

* **--epochs** -> Determina quantas épocas serão utilizadas para fazer o treinamento. (Valor padrão = 10000).
* **--lr** -> Determina o valor da taxa de aprendizado para o treinamento. (Valor padrão = 0.001).
* **--show-fig** -> Se esse parâmetro for passado, o programa vai gerar as imagens de erro e acurácia do treinamento. (Você precisa do pacote matplotlib para usar esse parâmetro).
* **--validation-split** -> Determina qual percentual das imagens de treino que serão utilizadas para a validação. (Valor padrão = 20).
* **--verbose** -> Determina de quantas em quantas épocas os resultados intermediários serão impressos no terminal. (Valor padrão = 500).
