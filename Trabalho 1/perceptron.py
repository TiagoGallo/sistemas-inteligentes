from random import random
import numpy as np

class Perceptron:
    def __init__(self, num_entries):
        '''
        Construtor da classe perceptron

        Entradas:
            num_entries - Recebe o numero de entradas que o perceptron vai ter,
nao contando o bias
        '''
        #inicializa a lista de pesos com valores aleatorios entre -0.1 e 0.1
        self.pesos = self.inicializa_pesos(num_entries)

    def inicializa_pesos(self, num_entries):
        '''
        Calcula valores aleatorios entre -0.1 e 0.1 para os pesos iniciais do perceptron
        '''
        #inicializa a lista de pesos
        pesos = []

        #Calcula pesos iniciais aleatorios para cada entrada e para o bias
        for _ in range(0, num_entries + 1):
            peso_aleatorio = random()           #gera um numero aleatorio entre 0.0 e 1.0
            
            #Normaliza o peso aleatorio entre valores de -0.1 e 0.1
            peso_aleatorio *= 0.2               
            peso_aleatorio -= 0.1
            
            pesos.append(peso_aleatorio)

        return pesos

    def predict(self, entrada):
        '''
        Dada uma entrada, faz a predicao
        '''
        #Multiplica as entradas pelos pesos e soma os resultados
        resultado = np.multiply(self.pesos, entrada)

        resultado = resultado.sum()

        #Passa a funcao de ativacao (funcao degrau)
        prediction = np.sign(resultado)

        if prediction >= 0.0:
            return 1.0
        else:
            return 0.0

    def update(self, trainingData, learningRate):
        '''
        Recebe uma lista que eh uma linha do conjunto de treinamento, faz a predicao e atualiza os 
pesos, caso necessario.
        '''
        #Separa os dados de treinamento entre entradas e valor esperado da saida
        entrada = trainingData[0:-1]
        
        #concatena a lista de entradas com um 1 para representar a entrada do bias
        entrada.append(1.0)

        #A saida esperada eh o ultimo elemento do vetor de dados de entrada
        saida_esperada = trainingData[-1]

        #Faz a predicao
        predicao = self.predict(entrada)

        #Calcula o erro
        erro = saida_esperada - predicao

        #Calcula o vetor de atualizacao dos pesos
        delta = np.multiply((learningRate * erro), entrada)

        #Atualiza os pesos com o vetor delta calculado
        self.pesos = np.add(self.pesos, delta).tolist()

        #Retorna o valor do erro
        return erro