import torch
import torch.nn.functional as F
from torchvision import datasets, transforms
import argparse
import numpy as np
import os
import matplotlib.pyplot as plt

def get_activation(model, x, layer_num):
    '''
    Gera imagens da ativacao 
    '''
    if layer_num == 1:
        output = model.first_convolution_layer(x)
    
    elif layer_num == 2:
        first_out = model.first_convolution_layer(x)                        
        second_out = model.first_pooling_layer(first_out)                   
        output = model.second_convolution_layer(second_out) 
    
    return output


def get_features_maps(model, layer_num, save, show):
    '''
    Gera as imagens de features maps da primeira camada convolucional
    '''
    if layer_num == 1:
        features_maps = model.first_convolution_layer.weight
    elif layer_num == 2:
        features_maps = model.second_convolution_layer.weight

    for j, mapa in enumerate(features_maps):
        mapa = mapa.cpu()
        mapa_np = mapa.detach().numpy()

        plt.figure()
        plt.title('Feature_map_{}_camada{}'.format(j, layer_num))
        plt.imshow(mapa_np[0], cmap='gray_r')

        if save:
            plt.savefig('./imagens/features_maps/Feature_map_{}_camada{}.png'.format(j, layer_num))
        
    if show:
        plt.show()


def prepare_test_loader():
    ''' 
    Prepara o objeto para carregar as imagens de teste
    '''

    test_dataset = datasets.MNIST('./data', 
                                train = False, 
                                transform = transforms.ToTensor())

    test_loader = torch.utils.data.DataLoader(dataset = test_dataset, 
                                                batch_size = 1, 
                                                shuffle = True)                            

    return test_loader

def generate_intermediate_outputs(test_loader, model, use_active_1, use_active_2, save, show):
    '''
    Gera as images de saidas intermediarias da rede
    '''

    for data, target in test_loader:   
        img = data.squeeze(0)       
        img = img.squeeze(0)

        plt.figure()
        plt.title("data")
        plt.imshow(img, cmap='gray_r')
        plt.savefig('./imagens/activations/imagem_original.png')

        if use_active_1:
            intemediate_output = get_activation(model, data.cuda(), 1)

            intemediate_output = intemediate_output.cpu()
            intemediate_output = intemediate_output.detach().numpy()
            activations = intemediate_output.squeeze(0)

            for j, activation in enumerate(activations):
                plt.figure()
                plt.title("activation_{}_camada_{}".format(j, 1))
                plt.imshow(activation, cmap='gray_r')
            
                if save:
                    plt.savefig('./imagens/activations/activation_{}_camada{}.png'.format(j, 1))

        if use_active_2:
            intemediate_output = get_activation(model, data.cuda(), 2)

            intemediate_output = intemediate_output.cpu()
            intemediate_output = intemediate_output.detach().numpy()
            activations = intemediate_output.squeeze(0)

            for j, activation in enumerate(activations):
                plt.figure()
                plt.title("activation_{}_camada_{}".format(j, 2))
                plt.imshow(activation, cmap='gray_r')
            
                if save:
                    plt.savefig('./imagens/activations/activation_{}_camada{}.png'.format(j, 2))

        if show:
            plt.show()
        
        break

def create_dirs():
    '''
    Cria os diretorios onde as imagens serao salvas
    '''
    if not os.path.isdir('./imagens'):
        os.mkdir('./imagens')

    if not os.path.isdir('./imagens/features_maps'):
        os.mkdir('./imagens/features_maps')

    if not os.path.isdir('./imagens/activations'):
        os.mkdir('./imagens/activations')

def main(args):
    '''
    Funcao principal do programa 
    '''
    #Carrega o modelo
    model = torch.load(args.model_name)

    if args.activations_1 or args.activations_2:
        test_loader = prepare_test_loader()

    if args.save:
        create_dirs()

    if args.feature_maps_1:
        get_features_maps(model, 1, args.save, args.show)

    if args.feature_maps_2:
        get_features_maps(model, 2, args.save, args.show)

    if args.activations_1 or args.activations_2:
        generate_intermediate_outputs(test_loader, model, args.activations_1, args.activations_2, args.save, args.show)


if __name__ == "__main__":
    #Create a argument parser and parse the arguments
    parser = argparse.ArgumentParser()

    parser.add_argument("--feature_maps_1", action = 'store_true',
                        help = "Mostra os features maps da primeira convolucao do modelo escolhido")
    parser.add_argument("--feature_maps_2", action = 'store_true',
                        help = "Mostra os features maps da segunda convolucao do modelo escolhido")
    parser.add_argument("--activations_1", action = 'store_true',
                        help = "Mostra as ativacoes da saida da primeira convolucao do modelo escolhido")
    parser.add_argument("--activations_2", action = 'store_true',
                        help = "Mostra as ativacoes da saida da segunda convolucao do modelo escolhido")
    parser.add_argument("--model_name", type = str, required = True, help = 'Nome do modelo para ser salvo')
    parser.add_argument("--show", action = 'store_true',
                        help = "Mostra as as imagens desejadas na tela")
    parser.add_argument("--save", action = 'store_true',
                        help = "Salva as imagens na pasta ./imagens")

    args = parser.parse_args()

    main(args)