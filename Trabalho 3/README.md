# Trabalho 3

Treinamento e teste de uma rede convolucional parecida com a LeNet na base de dados MNIST

## Dependências

Para rodar esses programas, precisa ter instalado

* python >= 3.5.2
* Numpy
* Matplotlib (se voce desejar gerar as imagens de erro e acurácia)
* torch
* torchvision

Instalação do pytorch: [link](https://pytorch.org/get-started/locally/)

### Uso - arquivo de treinamento

```bash

treinamento.py [-h] [--lr LR] [--epochs EPOCHS] [--verbose VERBOSE]
                    [--show-fig] [--validation-split VALIDATION_SPLIT]
                    [--batch BATCH] [--l1] [--l2]
                    [--norm-weight NORM_WEIGHT] [--momentum MOMENTUM]
                    [--data-augmentation]
                    [--n_mapas_camada1 N_MAPAS_CAMADA1]
                    [--n_mapas_camada2 N_MAPAS_CAMADA2] [--MLP_tam MLP_TAM]
                    [--save] [--model_name MODEL_NAME] [--summary]

```

A opção -h ou --help mostra todos os parâmetros possíveis para rodar esse programa e uma ajuda de pra que cada um serve

**Parâmetros**

* **--epochs** -> Determina quantas épocas serão utilizadas para fazer o treinamento. (Valor padrão = 20).
* **--lr** -> Determina o valor da taxa de aprendizado para o treinamento. (Valor padrão = 0.01).
* **--show-fig** -> Se esse parâmetro for passado, o programa vai gerar as imagens de erro e acurácia do treinamento. (Você precisa do pacote matplotlib para usar esse parâmetro).
* **--validation-split** -> Determina qual percentual das imagens de treino que serão utilizadas para a validação. (Valor padrão = 20).
* **--verbose** -> Determina de quantas em quantas épocas os resultados intermediários serão impressos no terminal. (Valor padrão = 12000).
* **--batch** -> Tamanho do batch de imagens para o treinamento.
* **--l1** -> Se esse parâmetro for acionado, será usada a regularização L1 (apenas na camada de saída da rede) durante o treinamento.
* **--l2** -> Se esse parâmetro for acionado, será usada a regularização L2 (apenas na camada de saída da rede) durante o treinamento.
* **--norm-weight** -> Peso associado a normalizacao L1 ou L2. (Valor padrão = 0.2) 
* **--momentum** -> Valor do momentum a ser adicionado no otmizador por descida de gradiente. (Valor padrão = 0).
* **--data-augmentation** -> Se ativado vai fazer algumas transformacoes nas imagens do dataset de treino, para simular que temos mais imagens.
* **--n_mapas_camada1** -> numero de mapas de features que a primeira camada convolucional vai gerar
* **--n_mapas_camada2** -> numero de mapas de features que a segunda  camada convolucional vai gerar
* **--MLP_tam** -> Numero de neuronios presentes do MLP final
* **--save** -> Se ativo vai salvar o modelo na pasta ./models
* **--model_name** -> Nome do modelo a ser salvo (nao necessita da extensao .pth)
* **--summary** -> Se ativo vai printar o sumario do modelo na tela

### Uso - arquivo de visualização das ativações e features maps

```bash

analyze_intermediate.py [-h] [--feature_maps_1] [--feature_maps_2]
                             [--activations_1] [--activations_2]
                             --model_name MODEL_NAME [--show] [--save]

```

A opção -h ou --help mostra todos os parâmetros possíveis para rodar esse programa e uma ajuda de pra que cada um serve

**Parâmetros**

* **--model_name** -> Parametro obrigatório, onde é necessário que você passe o path do modelo salvo
* **--show** -> Se ativo mostra as imagens na tela
* **--save** -> Se ativo salva as imagens na pasta ./imagens
* **--feature_maps_1** -> Se ativo calcula os features maps da primeira camada convolucional, o show/save vai definir se essas imagens serão salvas ou mostradas na tela
* **--feature_maps_2** -> Se ativo calcula os features maps da segunda camada convolucional, o show/save vai definir se essas imagens serão salvas ou mostradas na tela
* **--activations_1** -> Se ativo calcula as ativações da primeira camada convolucional, o show/save vai definir se essas imagens serão salvas ou mostradas na tela
* **--activations_2** -> Se ativo calcula as ativações da segunda camada convolucional, o show/save vai definir se essas imagens serão salvas ou mostradas na tela