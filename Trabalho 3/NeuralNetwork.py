import torch
import torch.nn as nn
import torch.nn.functional as F

class LeNet(nn.Module):
    '''
    Implementacao de uma rede convolucional parecida com a LeNet como descrita no trabalho
    '''
    def __init__(self, n_mapas_camada1, n_mapas_camada2, MLP_tam):
        super(LeNet, self).__init__()
        self.first_convolution_layer  = nn.Conv2d(1, n_mapas_camada1, (5, 5), bias=True)                    # 1 canal de entrada,         4 features maps de saida, tamanho 5x5 dos features maps
        self.first_pooling_layer      = nn.MaxPool2d((2, 2))                                                # Faz max pooling nos features maps com campos 2x2
        self.second_convolution_layer = nn.Conv2d(n_mapas_camada1, n_mapas_camada2, (5, 5), bias=True)      # 4 features maps de entrada, 6 features maps de saida, tamanho 5x5 dos features maps
        self.second_pooling_layer     = nn.MaxPool2d((2, 2))                                                # Faz max pooling nos features maps com campos 2x2
        self.fully_conected_layer     = nn.Linear(n_mapas_camada2 * 4 * 4, MLP_tam)                         # Primeiro layer fully conected
        self.final_layer              = nn.Linear(MLP_tam, 10)                                              # Layer fully connected final

    def forward(self, x):        
        x = self.first_convolution_layer(x)               # aplica a primeira camada convolucional         
        x = self.first_pooling_layer(x)                   # aplica a primeira camada de maxPooling         
        x = self.second_convolution_layer(x)              # aplica a segunda camada convolucional seguida de um maxPooling 2x2        
        x = self.second_pooling_layer(x)                  # aplica a segunda camada de maxPooling        

        x = x.view(x.size(0), -1)                         # Faz um flatten nos valores para preparar a entrada pra camada fully connected        
        x = F.tanh(self.fully_conected_layer(x))          # aplica a camada fully connected com uma ativacao tangente hiperbolica        
        x = F.log_softmax(self.final_layer(x), dim=1)     # aplica a segunda camada fully connected com ativacao softmax        
        
        return x