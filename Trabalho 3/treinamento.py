import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets, transforms
import argparse
import numpy as np
import os 
from datetime import datetime
from torchsummary import summary

from NeuralNetwork import LeNet

#Checa se ha uma GPU disponivel para fazer o treinamento, se sim seta ela para ser usada
if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

def load_data(batch_size, validation_split, use_data_augmentation):
    '''
    Carrega o dataset na memoria e prepara ele para o treinamento
    '''
    print("[INFO] Carregando o dataset")

    validation_split /= 100
    
    #Faz o download do dataset MNIST caso ainda nao tenha sido feito e o coloca na pasta ./data
    #Ja separa o dataset em treino e validacao utilizando o batch size definido
    if use_data_augmentation:
        train_dataset = datasets.MNIST('./data', 
                                train = True, 
                                download = True, 
                                transform = transforms.Compose([
                                    transforms.RandomAffine((-10, 10), translate=(.1, .1), scale=(.9, 1.1), shear=10),
                                    transforms.ToTensor()
                                ]))
    else:
        train_dataset = datasets.MNIST('./data', 
                                train = True, 
                                download = True, 
                                transform = transforms.ToTensor())

    test_dataset = datasets.MNIST('./data', 
                                train = False, 
                                transform = transforms.ToTensor())

    #Determina o tamanho dos datasets de validacao e treino
    lengths = [int(len(train_dataset) * (1 - validation_split)), int(len(train_dataset) * (validation_split))]  

    #Separa o dataset de treino em validacao e treino
    sub_train_dataset, sub_valid_dataset = torch.utils.data.random_split(train_dataset, lengths)                              

    #Cria objetos do pytorch que vao armazenar o dataset e fornecer os dados durante o treino, validacao e teste
    train_loader = torch.utils.data.DataLoader(dataset = sub_train_dataset, 
                                            batch_size = batch_size, 
                                            shuffle = True)
    
    valid_loader = torch.utils.data.DataLoader(dataset = sub_valid_dataset, 
                                            batch_size = batch_size, 
                                            shuffle = True)

    test_loader = torch.utils.data.DataLoader(dataset = test_dataset, 
                                            batch_size = batch_size, 
                                            shuffle = False)
    
    print("[INFO] Dataset Carregado")   
    return train_loader, valid_loader, test_loader                                 

def train(epochs, model, train_loader, optimizer, criterion, batch_size, valid_loader, args):
    '''
    Funcao para fazer o treinamento da rede
    '''
    #Cria as listas para armazenar os dados da acuracia e do erro durante a validacao
    lossv, accv = [], []

    #cria as listas para armazenar os dados de acuracia e erro durante o treino
    losst, acct = [], []

    for epoch in range(1, epochs + 1):
        #Prepara o modelo para ser treinado
        model.train()

        correct = 0
        acc_loss = 0
        
        # Loop pelos batches do conjunto de treinamento
        for batch_idx, (data, target) in enumerate(train_loader):
            # Envia os dados para GPU ou CPU
            data = data.to(device)
            target = target.to(device)

            # Zera os buffers do gradiente para um batch nao atrapalhar o outro
            optimizer.zero_grad() 
            
            # Passa os dados pela rede e gera uma predicao
            predict = model(data)

            # Calcula a funcao de custo (cross entropy)
            loss = criterion(predict, target)

            #adiciona regularizacao L1 a ultima camada
            if args.l1:
                l1_loss = args.norm_weight * torch.norm(model.final_layer.weight, p=1)
                loss += l1_loss
            #adiciona regularizacao L2 a ultima camada
            elif args.l2:
                l2_loss = args.norm_weight * torch.norm(model.final_layer.weight, p=2)
                loss += l2_loss

            acc_loss += loss

            # Backpropagation
            loss.backward()

            # Pega o indice da classe de maior probabilidade
            pred = predict.data.max(1)[1] 
            
            # calcula o numero de acertos
            correct += pred.eq(target.data).cpu().sum()
            
            # Atualiza os pesos
            optimizer.step()
            
            # Printa na tela o estado do treinamento de tempos em tempos
            if (batch_idx * batch_size) % args.verbose == 0:
                print('Train Epoch: {:02d} [{:05d}/{:05d} ({:.2f}%)]\tLoss: {:.4f}'.format(
                    epoch, batch_idx * len(data), len(train_loader.dataset),
                    100. * batch_idx / len(train_loader), loss.data.item()))
        
        #loss e acuracia da epoch
        acc_loss /= len(train_loader)
        losst.append(acc_loss)
        accuracy = 100. * correct.to(torch.float32) / len(train_loader.dataset)
        acct.append(accuracy)

        print("\nTrain Epoch {:02d}: Average Loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)".format(
            epoch, acc_loss, correct, len(train_loader.dataset), accuracy
        ))
        
        #Faz a validacao no fim de cada epoch do treinamento
        validate(lossv, accv, model, valid_loader, criterion)

    return lossv, accv, losst, acct

def validate(loss_vector, accuracy_vector, model, valid_loader, criterion):
    '''
    Funcao para fazer a validacao da rede a cada epoch e acompanhar a evolucao do treinamento
    '''
    # Prepara o modelo para fazer a validacao
    model.eval()

    # Inicializa algumas variaveis de suporte
    val_loss, correct = 0, 0

    # loop pelos batches das imagens de validacao
    for data, target in valid_loader:
        # Joga os dados para a GPU ou CPU
        data = data.to(device)
        target = target.to(device)

        #Gera a predicao da rede
        predict = model(data)

        #Calcula o erro (cross entropy)
        val_loss += criterion(predict, target).data.item()

        # Pega o indice da classe de maior probabilidade
        pred = predict.data.max(1)[1] 
        
        # calcula o numero de acertos
        correct += pred.eq(target.data).cpu().sum()

    #Divide o erro total pelo numero de imagens no dataset de validacao, para ter um erro medio por imagem
    val_loss /= len(valid_loader)
    loss_vector.append(val_loss)

    #Calcula o percentual de acertos no conjunto de validacao
    accuracy = 100. * correct.to(torch.float32) / len(valid_loader.dataset)
    accuracy_vector.append(accuracy)
    
    # Printa o resultado da validacao
    print('\nValidation set: Average loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)\n'.format(
        val_loss, correct, len(valid_loader.dataset), accuracy))

def test(model, test_loader, criterion):
    '''
    Funcao para avaliar a rede no conjunto de testes, que nunca foi usado para treinamento
    Essa funcao gera o resultado real da rede
    '''
    #Prepara a matriz de confusao
    confusion_matrix = np.zeros((10,10))

    # Prepara o modelo para fazer o teste
    model.eval()

    # Inicializa algumas variaveis de suporte
    val_loss, correct = 0, 0

    # loop pelos batches das imagens de teste
    for data, target in test_loader:
        # Joga os dados para a GPU ou CPU
        data = data.to(device)
        target = target.to(device)

        #Gera a predicao da rede
        predict = model(data)

        #Calcula o erro (cross entropy)
        val_loss += criterion(predict, target).data.item()

        # Pega o indice da classe de maior probabilidade
        pred = predict.data.max(1)[1] 
        
        # calcula o numero de acertos
        correct += pred.eq(target.data).cpu().sum()

        # Atualiza a matriz de confusao
        for predicao, ground_truth in zip (pred, target):
            confusion_matrix[predicao.item()][ground_truth.item()] += 1

    #Divide o erro total pelo numero de imagens no dataset de teste, para ter um erro medio por imagem
    val_loss /= len(test_loader)

    #Calcula o percentual de acertos no conjunto de teste
    accuracy = 100. * correct.to(torch.float32) / len(test_loader.dataset)
    
    # Printa o resultado do teste
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.2f}%)\n'.format(
        val_loss, correct, len(test_loader.dataset), accuracy))

    return confusion_matrix

def print_images(loss, acc, epochs, confusion_matrix, losst, acct):
    '''
    Funcao para mostrar as imagens do treinamento
    '''
    import matplotlib.pyplot as plt

    epochs_list = list(range(1, epochs + 1))

    #Imprime o historico do erro
    plt.figure(figsize=(5,3))
    plt.plot(epochs_list, loss, label = "validacao", color='red')
    plt.plot(epochs_list, losst, label = "treinamento", color='blue')
    plt.title('validation loss')
    plt.xlabel('epochs')
    plt.ylabel('Loss')
    plt.legend()

    #Imprime o historico da acuracia
    plt.figure(figsize=(5,3))
    plt.plot(epochs_list, acc, label = "validacao", color='red')
    plt.plot(epochs_list, acct, label = "treinamento", color='blue')
    plt.title('validation accuracy')
    plt.xlabel('epochs')
    plt.ylabel('Accuracy')
    plt.legend()

    #Imprime a matriz de confusao
    fig, ax = plt.subplots()
    im = ax.imshow(confusion_matrix, cmap='binary')

    # We want to show all ticks...
    ax.set_xticks(np.arange(0,10))

    ax.set_yticks(np.arange(0,10))
    
    # ... and label them with the respective list entries
    ax.set_xticklabels([0,1,2,3,4,5,6,7,8,9])
    ax.set_yticklabels([0,1,2,3,4,5,6,7,8,9])

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=0, ha="right",
            rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(10):
        for j in range(10):
            if i == j:
                text = ax.text(j, i, int(confusion_matrix[i, j]),
                        ha="center", va="center", color="w")
            else:
                text = ax.text(j, i, int(confusion_matrix[i, j]),
                        ha="center", va="center", color="black")

    plt.xlabel('Classe verdadeira')
    plt.ylabel('Classe predita')
    ax.set_title("Matriz de confusao")
    fig.tight_layout()
    plt.show()

def load_model(learning_rate, momentum, n_mapas_camada1, n_mapas_camada2, MLP_tam):
    '''
    Funcao para carregar a rede escolhida, determinar o otimizador e a funcao de custo
    '''
    print("[INFO] Carregando o modelo")
    
    #cria a rede neural, seleciona o otimizador e o erro
    model = LeNet(n_mapas_camada1, n_mapas_camada2, MLP_tam).to(device)
    
    optimizer = torch.optim.SGD(model.parameters(), lr = learning_rate, momentum = momentum)
    criterion = nn.CrossEntropyLoss()
    
    print("[INFO] Modelo carregado")

    return model, optimizer, criterion

def save_model(model, name):
    '''
    Salva o modelo treinado para poder ser recuperado posteriormente
    '''

    if not os.path.isdir('./models'):
        os.mkdir('./models')

    if name is None:
        aux = datetime.now()

        model_name = "./models/({}-{}-{})_{}:{}:{}.pth".format(aux.day, aux.month, aux.year, aux.hour, aux.minute, aux.second)

    else:
        model_name = "./models/"
        model_name = model_name + name + '.pth'

    torch.save(model, model_name)

def main(args):
    '''
    Funcao principal do programa
    '''
    #carrega os dados do Dataset
    train_loader, valid_loader, test_loader = load_data(args.batch, args.validation_split, args.data_augmentation)

    #carrega a rede neural
    model, optimizer, criterion = load_model(args.lr, args.momentum, args.n_mapas_camada1, args.n_mapas_camada2, args.MLP_tam)

    
    #treina o modelo
    lossv, accv, losst, acct = train(args.epochs, model, train_loader, optimizer, criterion, args.batch, valid_loader, args)
    
    #Avalia o modelo no conjunto de testes
    confusion_matrix = test(model, test_loader, criterion)

    if args.summary:
        summary(model, (1,28,28))

    if args.save:
        save_model(model, args.model_name)

    #mostra os graficos do erro e da acuracia durante o treinamento
    if args.show_fig:
        print_images(lossv, accv, args.epochs, confusion_matrix, losst, acct)

if __name__ == "__main__":
    #Create a argument parser and parse the arguments
    parser = argparse.ArgumentParser()

    #Variables for the video source
    parser.add_argument("--lr", type = float, help = "O valor da taxa de aprendizado (learning rate) para realizar o treinamento",
                        default = 0.01)
    parser.add_argument("--epochs", type = int, help = "O numero maximo de epocas que o treinamento pode realizar", 
                        default = 20)
    parser.add_argument("--verbose", type = int, default = 12000,
                        help = "Numero que define de quantas em quantas epocas o usuario quer um print de feedback do programa")
    parser.add_argument("--show-fig", action = 'store_true',
                        help = "Se esse argumento for passado, o programa vai gerar imagens sobre a acuracia e o erro durante o treinamento")
    parser.add_argument("--validation-split", type = int, default = 20,
                        help = "Define qual percentual do conjunto de treinamento deve ser usado para a validacao")
    parser.add_argument("--batch", "-b", type = int, default = 32, help = "Batch size to training")
    parser.add_argument("--l1", action = "store_true", help = "Se ativado, sera utilizada a regularizacao L1 na ultima camada")
    parser.add_argument("--l2", action = "store_true", help = "Se ativado, sera utilizada a regularizacao L2 na ultima camada")
    parser.add_argument("--norm-weight", type = float, default = 0.0001, help = "Peso associado a normalizacao L1 ou L2")
    parser.add_argument("--momentum", type = float, default = 0, help = "momentum utilizado no otimizador SGD")
    parser.add_argument("--data-augmentation", action = "store_true", help = "Se ativado vai fazer algumas transformacoes nas imagens do dataset"
                        " de treino, para simular que temos mais imagens")
    parser.add_argument("--n_mapas_camada1", type = int, default = 4, help = "numero de mapas de features que a primeira camada convolucional"
                        " vai gerar")                        
    parser.add_argument("--n_mapas_camada2", type = int, default = 6, help = "numero de mapas de features que a segunda  camada convolucional"
                        " vai gerar")                        
    parser.add_argument("--MLP_tam", type = int, default = 500, help = "Numero de neuronios presentes do MLP final")
    parser.add_argument("--save", action = "store_true", help = "Se ativo vai salvar o modelo na pasta ./models")
    parser.add_argument("--model_name", type = str, default = None, help = 'Nome do modelo para ser salvo')
    parser.add_argument("--summary", action = "store_true", help = "Se ativo vai printar o sumario do modelo na tela")

    args = parser.parse_args()

    main(args)